package com.epam.rd.autotasks.triangle;

class Triangle {

    private final double x1;
    private final double y1;
    private final double x2;
    private final double y2;
    private final double x3;
    private final double y3;

    public Triangle(Point a, Point b, Point c) {
        this.x1 = a.getX();
        this.y1 = a.getY();
        this.x2 = b.getX();
        this.y2 = b.getY();
        this.x3 = c.getX();
        this.y3 = c.getY();

        double area = 0.5d*(x1 * (y2 - y3) +
                x2 * (y3 - y1) +
                x3 * (y1 - y2));

        if (area==0) {
            throw new IllegalArgumentException();
        }
    }

    public double area() {
        double area = 0.5d*(x1 * (y2 - y3) +
                x2 * (y3 - y1) +
                x3 * (y1 - y2));

        if (area==0) {
            throw new IllegalArgumentException();
        } else {
            return Math.abs(area);
        }
    }

    public Point centroid(){
        var x = (x1 + x2 + x3) / 3;
        var y = (y1 + y2 + y3) / 3;
        return new Point(x,y);
    }

}
